# Responses Handler

_Provee de métodos con la estructura de respuesta status, message y content_

### Instalación 

```
composer require skievacd/pb-responses-handler
```

### Uso 

_Utilice los métodos estáticos success, info, warning y error de la clase Responses, pasando como argumentos opcionales el message y/o el content"_


```
use ResponsesHandler\Responses;

 $response = Responses::info("un mensaje de información");

```
