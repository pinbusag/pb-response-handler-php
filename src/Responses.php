<?php

namespace ResponsesHandler;

class Responses {


    public static function success($message = "", $content = null){
        $response = [
            "success" =>  true,
            "message" => $message,
            "content" => $content
        ];
        return $response;
    }

    public static function info($message = "", $content = null){
        $response = [
            "success" => true,
            "message" => $message,
            "content" => $content
        ];
        return $response;
    }

    public static function warning($message = "", $content = null){
        $response = [
            "success" => true,
            "message" => $message,
            "content" => $content
        ];
        return $response;
    }

    public static function error($message = "", $content = null){
        $response = [
            "success" => false,
            "message" => $message,
            "content" => $content
        ];
        return $response;
    }

}